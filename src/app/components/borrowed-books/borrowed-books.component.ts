import { Component, OnInit } from '@angular/core';
import { BookService } from 'src/app/services/book.service';
import { Book } from 'src/app/models/books.model';

@Component({
  selector: 'app-borrowed-books',
  templateUrl: './borrowed-books.component.html',
  styleUrls: ['./borrowed-books.component.scss']
})
export class BorrowedBooksComponent implements OnInit {

  books: Array<Book>;
  booksList: Array<Book>;
  markButtonText = 'Mark as Returned';

  constructor(
    private bookService: BookService
  ) { }

  ngOnInit() {
    this.getAllBooks()
  }

  getAllBooks() {
    this.bookService.getBooks().subscribe(res => {
      this.booksList = res;
      this.books = this.booksList.filter(book => book.status !== 'In-Shelf');
      console.log(this.books);
    }, err => {
      console.log(err);
    });
  }

  updateStatus(book) {
    this.bookService.updateBook(book.id, { status: 'In-Shelf' }).subscribe(res => {
      this.getAllBooks();
    }, err => {
      console.log(err);
    })
  }

  deleteBook(id) {
    this.bookService.deleteBook(id).subscribe(res => {
      console.log(res);
    }, err => {
      console.log(err);
    })
  }

}
