import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BookService } from 'src/app/services/book.service';
import { Genres } from 'src/app/Genres';
import { Sections } from 'src/app/Sections';
import { BooksComponent } from '../books/books.component';

@Component({
  selector: 'my-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  genres;
  sections;
  title = `Rio's Library`;
  routes = [
    {
      name: 'Home',
      link: '/'
    },
    {
      name: 'Borrowed Books',
      link: '/borrowed'
    }
  ]
  constructor(
    private books: BookService
  ) { }


  ngOnInit() {
    this.genres = Genres;
    this.sections = Sections;
  }

  addBook(f: NgForm) {
    this.books.addBook(f.value).subscribe(res => {
      alert('Successfully Added Book.');
    }, err => {
      console.log(err);
    });
  }

}
