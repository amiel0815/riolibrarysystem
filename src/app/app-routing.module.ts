import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './components/books/books.component';
import { BookUpdateComponent } from './components/book-update/book-update.component';
import { BorrowedBooksComponent } from './components/borrowed-books/borrowed-books.component';



const routes: Routes = [
  { 
    path: '',
    component: BooksComponent
  },
  {
    path: 'update/:id',
    component: BookUpdateComponent
  },
  {
    path: 'borrowed',
    component: BorrowedBooksComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
